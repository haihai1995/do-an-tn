import React, { Component } from 'react'
import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom'
import { CartProvider } from './context/Cart';
import Client from './ViewApp/Client'
import Admin from './ViewApp/Admin'

class App extends Component {
  render() {
    return (
    <div>
      <CartProvider>
        <Router>
          <Switch>
              <Route path="/" exact component={Client} />
              <Route path="/Admin" exact component={Admin} />
          </Switch>
        </Router>
      </CartProvider>
    </div>
    )
  }
}

export default App;




