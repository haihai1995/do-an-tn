/* eslint-disable react/prop-types,react/sort-comp */
import React from 'react';
const CartContext = React.createContext({});
export class CartProvider extends React.Component {
  addProduct = (product) => {
    const cart = this.getCart();
    const newProduct = {
      ...product,
      count: 1,
      
    };
    const filtered = cart.filter(item => item._id === newProduct._id);
    if (filtered.length > 0) {
      const pos = cart.map(i => i._id).indexOf(newProduct._id);
      cart[pos].count += 1;
    } else {
      cart.push(newProduct);
    }
    sessionStorage.setItem('cart', JSON.stringify(cart));
    this.setState({
      cart,
      count: this.getCount(),
    });
  };

  decrement = (product) => {
    const cart = this.getCart();
    const newProduct = {
      ...product,
      count: 1,
      
    };
    const filtered = cart.filter(item => item._id === newProduct._id);

    if (filtered.length > 0 ) {
      const pos = cart.map(i => i._id).indexOf(newProduct._id);
      cart[pos].count -= 1;
    } else {
      cart.push(newProduct);
    }

    sessionStorage.setItem('cart', JSON.stringify(cart));
    this.setState({
      cart,
      count: this.getCount(),
    });
  };

  handleDelete = (product) =>{
    const cart = this.getCart();
    const newProduct = {
      ...product,
      count: '',
    };
    const filtered = cart.filter(item => item._id === newProduct._id);
    if (filtered.length > 0) {
      let tempCart = [...this.state.cart];
      const pos = cart.map(i => i._id).indexOf(newProduct._id);
      cart [pos].getCount = false
      cart [pos].count = 0
    } else {
      cart.push(newProduct);
    }
    sessionStorage.setItem('cart', JSON.stringify(cart));
    this.setState({
      cart,
      count: this.getCount(),
    });
};

  
  getCart = () => (sessionStorage.getItem('cart') ? JSON.parse(sessionStorage.getItem('cart')) : []);

  getCount = () => {
    let count = 0;
    const cart = this.getCart();

    if (cart.length > 0) {
      cart.forEach((item) => {
        count += item.count;
      });
    }
    return count;
  };

  getTotal = () => {
    const { cart } = this.state;
    return cart.length ? cart.reduce((acc, item) => (
      acc + item.price * item.count
    ), 0).toFixed(2) : Number(0).toFixed(2);

  };

  state = {
    cart: this.getCart(),
    count: this.getCount(),
    addProduct: this.addProduct,
    getTotal: this.getTotal,
    handleDelete: this.handleDelete,
    decrement: this.decrement,
    clearCart: this.decrement
  };



  render() {
    const { children } = this.props;
    return (
      <CartContext.Provider value={this.state}>
        {children}
      </CartContext.Provider>
    );
  }
}

export default CartContext;