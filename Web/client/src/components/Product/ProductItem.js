import React from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Image } from 'react-bootstrap';
import '../../css/productItem.css'
import Ranting from './Ranting';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'
import swal from 'sweetalert'


function ProductItem(props) {
  const {
    image, currency, price, name, onBuy, onItemClick,complete, status,amounts
  } = props;
  return (
    <div className="mt-4">
      <Card className="cart">
        <div className="img-container p-5">
          <Image className="card-img-top" src={image}  />   
          <Button className="cart-btn" variant="danger" onClick={onBuy}><FontAwesomeIcon icon={faCartPlus}/></Button>
        </div>           
          <Card.Body className="card-footer justify-content-between">
            <div >
              <Card.Title>
                <p onClick={onItemClick} style={{cursor: 'pointer'}} className={complete ? 'completed' : ''}>
                    {name}
                </p>
              </Card.Title>
              <Card.Text className="text-danger">
                {price}&nbsp;{currency}
              </Card.Text>
              <Card.Text className="">
                {status}
              </Card.Text>
            </div>
            <Ranting/>
          </Card.Body>
      </Card>
    </div>
  );
}

ProductItem.propTypes = {
  image: PropTypes.string.isRequired,
  currency: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onBuy: PropTypes.func.isRequired,
  onItemClick: PropTypes.func.isRequired,
};


export default ProductItem;