import React from 'react';
import PropTypes from 'prop-types';
import ProductItem from './ProductItem';

function ProductList(props) {
  const { list, onBuy } = props;
  return (
    <div className="d-flex flex-wrap" style ={{marginLeft:-15,marginRight:-15}}>
      {list.map(item => (
        <div key={item._id} className="col-md-3 col-xs-6">
          <ProductItem
            key={item.id}
            name={item.name}
            price={item.price}
            currency={item.currency}
            description={item.description}
            image={item.image}
            status={item.status}
            amounts={item.amounts}
            onBuy={() => onBuy(item)}
            onItemClick={() => props.onItemClick(item._id)}
            complete={!!item.complete}
          />
        </div>
      ))}
    </div>
  );
}
// eslint -disable-next-line react/no-typos
ProductList.propTypes = {
  list: PropTypes.array.isRequired,
  onBuy: PropTypes.func.isRequired,
  onItemClick: PropTypes.func.isRequired,
};

export default ProductList;