import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Product extends Component {

    onBuy = (product) => {
        this.context.addProduct(product);
      };
    
    render() {
        return (
            <div className="item col-xs-6 col-lg-3 filter apple"><hr />
                <div className="thumbnail">
                    <img className="group list-group-image img-thumbnail img-fluid image" src={ this.props.obj.image } alt={ this.props.obj.name } />
                    <div className="caption group-sp">
                        <h6 className="group inner list-group-item-heading"> { this.props.obj.name } </h6>
                        <p className="group inner list-group-item-text"> <del> { this.props.obj.price } </del><sup>đ</sup></p>
                        <div className="row">
                           
                            <div className="col-xs-12 col-md-5">
                                <Link to={"/detail/"+this.props.obj._id} className="btn btn-primary">Chi tiết</Link>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;