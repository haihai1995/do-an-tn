import React , {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faSignInAlt, faDownload } from '@fortawesome/free-solid-svg-icons'
import '../../css/Header.css'
import { Link, withRouter } from 'react-router-dom';
// import Modal from 'react-responsive-modal';
import {register} from '../SignIn/UserFunctions';
import { login } from '../SignIn/UserFunctions';
 

class Landing extends Component {
  constructor(props) {
    super(props)
    this.state = {

      username:'',
      email:'',
      password:'',
      error:{},
      sign: false,
      login: false,
      first_name: '',
      last_name: '',
    }
  }

  onChange =(e)=>{
    this.setState({ [e.target.name]: e.target.value })
  };
  onSubmit =(e)=>{
    e.preventDefault()
    const newUser = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    }
    register(newUser).then(res => {
      this.props.history.push(`/login`)
    })
  };

  onSubmitLogin=(e)=> {
    e.preventDefault()
    const user = {
      email: this.state.email,
      password: this.state.password
    }

    login(user).then(res => {
      if (res) {
        this.props.history.push(`/profile`)
      }
    })
  }

  logOut(e) {
    e.preventDefault()
    localStorage.removeItem('usertoken')
    this.props.history.push(`/`)
  }

  
  render() {
    // const { login, sign } = this.state;
    const loginRegLink = (
      <div> 
        <button id="login" className="topBarlink"><Link to ="/Login" style={{color:'white'}}><FontAwesomeIcon icon={faSignInAlt}/>&nbsp; Đăng Nhập</Link></button>
        <button id="login" className="topBarlink"><Link to ="/Register" style={{color:'white'}}><FontAwesomeIcon icon={faUser}/>&nbsp; Đăng Ký</Link></button>
        {/* {localStorage.usertoken ? userLink : loginRegLink}        */}
      </div>
    )
    const userLink = (
      <div className="Users">
          <button>
            <Link to="/profile" className="nav-link-item text-white">
              Thông Tin
            </Link>
          </button>
          <button onClick={this.logOut.bind(this)} className="nav-link-item text-white">
            <Link to="/client" className="nav-link-item text-white">
              Đăng Xuất
            </Link>
          </button>
      </div>
    )
    return (
      <div> 
        <section className="topBar">
            <div className="container">
              <div className="row">
                <div className="topBarCol_2wvN topBarColLeft_2fRx">
                  <button id="tai_app" className="topBarLink_2Ybi" rel="nofollow"><FontAwesomeIcon icon={faDownload}/>&nbsp;Tải ứng dựng</button>
                  <button id="customer_service" className="topBarLink_2Ybi" rel="nofollow">Chăm sóc khách hàng</button>
                  <button id="checkOrder" className="topBarLink_2Ybi" rel="nofollow">Kiểm tra đơn hàng</button>
                </div>
              <div className="topBarLink">
                {localStorage.usertoken ? userLink : loginRegLink}
              </div>
              </div>
            </div>
        </section>
      </div>
    )
  }
}

export default withRouter(Landing)