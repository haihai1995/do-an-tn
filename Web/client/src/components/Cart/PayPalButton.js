import React from 'react';
import PropTypes from 'prop-types';
import CartItem from './CartItem';
import '../../css/Cart.css'
import Cartorder from './Cart'

function CartList(props) {
  const { list } = props;
  return (
    <div className="bp-list" style={{backgroundColor:"white"}}>
      {list.map(item => (
        <div key={item._id} style={{ padding: 15 }}>
          <Cartorder
            key={item.id}
            price={item.price}
            currency={item.currency}
            name={item.name}
            count={item.count}
            image={item.image}
          />
        </div>
      ))}
    </div>
  );
}

CartList.propTypes = {
  list: PropTypes.array.isRequired,
  removeItem :PropTypes.func.isRequired
};

export default CartList;