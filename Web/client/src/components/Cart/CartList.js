import React from 'react';
import PropTypes from 'prop-types';
import CartItem from './CartItem';
import '../../css/Cart.css'

function CartList(props) {
  const { list , removeItem , increment, decrement} = props;
  return (
    <div className="bp-list" style={{backgroundColor:"white"}}>
      {list.map(item => (
        <div key={item._id} style={{ padding: 15 }}>
          <CartItem
            key={item.id}
            price={item.price}
            currency={item.currency}
            name={item.name}
            count={item.count}
            description={item.description}
            image={item.image}
            removeItem={() => removeItem(item)}
            increment={() => increment(item)}
            decrement={() => decrement(item)}
          />
        </div>
      ))}
    </div>
  );
}

CartList.propTypes = {
  list: PropTypes.array.isRequired,
  removeItem :PropTypes.func.isRequired
};

export default CartList;