import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'
function Cartorder(props) {
  const {
    image, currency, price, name, count
  } = props;


  return (
    <div className="d-flex row text-center">
      <Image style={{ width: 100}} className="img-fluid" src={image} />
    <div className="d-flex flex-column col-4 " style={{marginTop:40}}>
      <div>
        {name}
      </div>
    </div>
    <div className="col-2"style={{marginTop:40}}>
      {price}&nbsp;&nbsp;{currency}
    </div>
    <div className="col-10 mx-auto col-lg-2"style={{marginTop:35}}>
      <div className="d-flex justify-content-center">
        <div>
       
          <span className="btn btn-black">{count}</span>
  
        </div>
      </div>
    </div>

  </div>
  );
}
Cartorder.propTypes = {
  image: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,

};

export default Cartorder;