import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'


function CartTotal(props) {
  const {
    total, onSubmit
  } = props;
  return (
    <div className="mt-3">
      <div style={{textAlign:"right"}}>
        <h2 className="text">Tổng tiền: <span className="" style={{color:"red"}}>{total}đ</span></h2>
      </div>
    </div>
  );
}

CartTotal.propTypes = {
  total: PropTypes.string.isRequired,
};

export default CartTotal;