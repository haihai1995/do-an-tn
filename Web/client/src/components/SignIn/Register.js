import React, { Component } from 'react'
import { register } from './UserFunctions'
import '../../css/login.css'


const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);
const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

class Register extends Component {
  constructor() {
    super()
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      formErrors: {
        firstName: "",
        lastName: "",
        email: "",
        password: ""
      }
    }
  }

  onChange =(e)=> {
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };
    switch (name) {
      case "firstName":
        formErrors.firstName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "lastName":
        formErrors.lastName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "email":
        formErrors.email = emailRegex.test(value)
          ? ""
          : "invalid email address";
        break;
      case "password":
        formErrors.password =
          value.length < 6 ? "minimum 6 characaters required" : "";
        break;
      default:
        break;
    }

    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };
  

  onSubmit =(e)=> {
    e.preventDefault()
    const newUser = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    }
    register(newUser).then(res => {
      this.props.history.push(`/login`)
    })
  }

  render() {
    const { formErrors } = this.state;
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h1>Create Account</h1>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="firstName">
                <label htmlFor="firstName">Họ</label>
                <input
                  type="text"
                  className={formErrors.firstName.length > 0 ? "error" : null}
                  name="first_name"
                  placeholder="First name"
                  value={this.state.first_name}
                  onChange={this.onChange}
                />
                {formErrors.firstName.length > 0 && (
                <span className="errorMessage">{formErrors.firstName}</span>
              )}
              </div>
              <div className="lastName">
                <label htmlFor="lastName">Tên Và Đệm</label>
                <input
                  type="text"
                  className={formErrors.lastName.length > 0 ? "error" : null}
                  name="last_name"
                  placeholder="Last name"
                  value={this.state.last_name}
                  onChange={this.onChange}
                />
                 {formErrors.lastName.length > 0 && (
                <span className="errorMessage">{formErrors.lastName}</span>
              )}
              </div>
              <div className="email">
                <label htmlFor="email">Địa chỉ email</label>
                <input
                  type="email"
                  className={formErrors.email.length > 0 ? "error" : null}
                  name="email"
                  placeholder="Enter email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
                {formErrors.email.length > 0 && (
                <span className="errorMessage">{formErrors.email}</span>
              )}
              </div>
              <div className="password">
                <label htmlFor="password">Mật Khẩu</label>
                <input
                  type="password"
                  className={formErrors.password.length > 0 ? "error" : null}
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                  {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
              </div>
              <div className="createAccount">
                <button type="submit">Create Account</button>
                
            </div>
            </form>
        </div>
      </div>
    )
  }
}

export default Register
