import React, { Component } from 'react';
import DogList from './DogList';
import Seachform from './seachform'
import axios from 'axios'

class DogApp extends Component {
    constructor(props) {
        super(props); 
            super(props);
            this.state ={
                products:[],
                searchDog:''
            }
        }
handleInput = (e)=>{
    console.log(e.target.value);
    
    this.setState({searchDog: e.target.value})
}
async componentDidMount() {
    axios.get(`http://localhost:5000/products`)
      .then(res => {
        this.setState({
            products: res.data
        })
    })
}

  render() {
      let filteredDogs = this.state.products.filter((dog)=>{
          return dog.name.toLowerCase().includes(this.state.searchDog.toLowerCase())
      }
    )
    return (
      <div className="App">
         <h1>Dog Images</h1>
         <Seachform handleInput={this.handleInput}/>
         <DogList filteredDogs = {filteredDogs}/> 
        </div>
    );
  }
}

export default DogApp;
