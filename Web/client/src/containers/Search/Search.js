// import React, {Component} from 'react';
// import {Link} from "react-router-dom";
// import Seachform from './seachform'
// import DogList from './DogList';

// export default class Search extends Component {
//     state = {
//         Product: [],
//         query: "",
//         filteredData: [],
//     };

//     handleInputChange = event => {
//         const query = event.target.value;
//         this.setState(prevState => {
//             const filteredData = prevState.Product.filter(element => {
//                 return element.name.toLowerCase().includes(query.toLowerCase());
//             });
//             return {
//                 query,
//                 filteredData
//             };
//         });
//     };

//     getData = () => {
//         fetch(`http://localhost:5000/products`)
//             .then(response => response.json())
//             .then(Product => {
//                 const {query} = this.state;
//                 const filteredData = Product.filter(element => {
//                     return element.name.toLowerCase().includes(query.toLowerCase());
//                 });

//                 this.setState({
//                     Product,
//                     filteredData
//                 });
//             });
//         };

//     componentWillMount() {
//         this.getData();
//     }

//     render() {
//         return (
//             <div className="container">
//                 <form className="ml-md-3">
//                     <div className="form-group text-left bg-light border-top border-bottom p-2 mt-2 mb-2 row">
//                         <label htmlFor="inputName" className="col-sm-auto col-form-label">Nhập tên sản phẩm cẩn tìm</label>
//                         <div className="col-sm-9">
//                             <input type="name"
//                                 className="form-control"
//                                 id="inputName"
//                                 placeholder="Search ..."
//                                 value={this.state.query}
//                                 onChange={this.handleInputChange}
//                             />
//                         </div>
//                     </div>
//                 </form>
//                 {/* <DogList/> */}
//                 <div className="">
//                     {this.state.filteredData.map((i , index) =>
//                         <div className="">
//                             {i.name}
//                         </div>
//                     )}
//                 </div>
//             </div>
//         )
//     }
// }

// import React, { useState } from 'react';
// import axios from 'axios'
// function Search() {
//   // Declare a new state variable, which we'll call "count"
//   const [count, setCount] = useState("con hang");

//   return (
//     <div>
//       <p>You clicked {count} times</p>
//       <button onClick={() => setCount("hết hàng")}>
//         Click me
//       </button>
//     </div>
//   );
// }

// export default Search;



import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      { date: '2020-01-05', customerId: '11091700', amount: 3 },
      { date: '2020-01-02', customerId: 'Anonymous', amount: 1 },
    ],
  };
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.name}
        </TableCell>
        <TableCell align="right">{row.calories}</TableCell>
        <TableCell align="right">{row.fat}</TableCell>
        <TableCell align="right">{row.carbs}</TableCell>
        <TableCell align="right">{row.protein}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                History
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Date</TableCell>
                    <TableCell>Customer</TableCell>
                    <TableCell align="right">Amount</TableCell>
                    <TableCell align="right">Total price ($)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    calories: PropTypes.number.isRequired,
    carbs: PropTypes.number.isRequired,
    fat: PropTypes.number.isRequired,
    history: PropTypes.arrayOf(
      PropTypes.shape({
        amount: PropTypes.number.isRequired,
        customerId: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
      }),
    ).isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    protein: PropTypes.number.isRequired,
  }).isRequired,
};

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 3.99),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 4.99),
  createData('Eclair', 262, 16.0, 24, 6.0, 3.79),
  createData('Cupcake', 305, 3.7, 67, 4.3, 2.5),
  createData('Gingerbread', 356, 16.0, 49, 3.9, 1.5),
];

export default function CollapsibleTable() {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Dessert (100g serving)</TableCell>
            <TableCell align="right">Calories</TableCell>
            <TableCell align="right">Fat&nbsp;(g)</TableCell>
            <TableCell align="right">Carbs&nbsp;(g)</TableCell>
            <TableCell align="right">Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
