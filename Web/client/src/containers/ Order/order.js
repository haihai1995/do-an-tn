import React, {Component} from 'react';
import axios from 'axios';
import '../../css/order.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import CartList from '../../components/Cart/PayPalButton';
import CartTotal from '../../components/Cart/CartTotalOrder';
import CartContext from '../../context/Cart';


export default class Order extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this)
        this.state = {
            name: '',
            phone: '',
            province:'',
            ward:'',
            address:''
        }
    }
    onChangeName = (e) => {
        this.setState({
            name: e.target.value
        });
    }

    onChangePrice = (e) => {
        this.setState({
            phone: e.target.value
        });
    }

    onChangeImage = (e) => {
        this.setState({
            province: e.target.value
        });
    }

    onChangeDetail = (e) => {
        this.setState({
            ward: e.target.value
        });
    }

    onChangeAddress = (e) => {
      this.setState({
          address: e.target.value
      });
    }

    onSubmit(e) {
        e.preventDefault();
        const obj = {
            price: this.price,
            name: this.state.name,
            phone: this.state.phone,
            province: this.state.province,
            ward: this.state.ward,
            address: this.state.address,
            name: this.state.name

        };
        axios.post('http://localhost:5000/order', obj)
            .then(res => console.log(res.data));
        this.setState({
            name: '',
            phone: '',
            province: '',
            ward:'',
            address:'',
            price: "",
            name:''
        })
    }

    render() {
        return (
        <div className="list-item-app"style={{backgroundColor:"ghostwhite"}}>
          <div className="container">
            <div className="list-item-1" style={{backgroundColor: "white"}}>
              <nav className="checkout">
                <ul className="breadcrumb"style={{backgroundColor:"white", fontSize:10, color:"darkgray"}}>
                  <li className="item_1wt2">Shopping&nbsp;</li>
                  <li className="item_1wt3"><FontAwesomeIcon icon={faChevronRight}/>&nbsp;Giỏ Hàng&nbsp;</li>
                  <li className="item_1wt4"><FontAwesomeIcon icon={faChevronRight}/>&nbsp;Thanh Toán</li>
                </ul>
                <h4 className="" style={{marginLeft:15,fontSize:15}}>ĐỊA CHỈ NHẬN HÀNG</h4>
              </nav>
            </div>
            <div className="">
              <div className="list-item" style={{marginTop: 10, }}>
                  <form onSubmit={this.onSubmit} className="form-list">
                    <CartContext.Consumer>
                        {({ cart, count, getTotal }) => {
                            console.log(cart)
                            return (
                            <div>
                            <div className="d-flex cart-wrapper">
                                <div style={{ flex: 1 }}>
                                <CartList list={cart}
                                />
                                </div>
                            </div>
                                <div className="" style={{ marginLeft: 15 }}>
                                <CartTotal total={getTotal()}
                                />
                                </div>
                            </div>
                            );
                        }}
                        </CartContext.Consumer>
                      <div className="form-group">
                          <label for="htmlFor">HỌ TÊN NGƯỜI NHẬN : </label>
                          <input type="text" className="form-control"
                              placeholder="Họ tên"
                              value={this.state.name}
                              onChange={this.onChangeName} />
                      </div>
                      <div className="form-group">
                          <label for="htmlFor">SỐ ĐIỆN THOẠI: </label>
                          <input type="text" className="form-control"
                              placeholder = "Số điện thoại khi giao hàng"
                              value={this.state.phone}
                              onChange={this.onChangePrice} />
                      </div>
                      <div className="form-group">
                          <label for="htmlFor">TỈNH THÀNH/ QUẬN HUYỆN : </label>
                          <input type="text" className="form-control"
                              placeholder = "Chọn Tỉnh Thành/Quận huyện"
                              value={this.state.province}
                              onChange={this.onChangeImage} />
                      </div>
                      <div className="form-group">
                          <label for="htmlFor">PHƯỜNG / XÃ: </label>
                          <input type="text" className="form-control"
                              placeholder = "Chọn phường / Xã"
                              value={this.state.ward}
                              onChange={this.onChangeDetail} />
                      </div>
                      <div className="form-group">
                          <label for="htmlFor">ĐỊA CHỈ : </label>
                          <input type="text" className="form-control"
                              placeholder = "Nhập số nhà tên đường"
                              value={this.state.address}
                              onChange={this.onChangeAddress} />
                      </div>
                      <div className="form-group custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck" name="example1"/>
                        <label className="custom-control-label" for="customCheck">Đặt làm địa chỉ mặc định</label>
                      </div>
                      <div className="form-custom">
                          <button onClick={this.onSubmit} value="Tiếp tục" className="btn btn-primary" style={{backgroundColor:"red"}}> Tiếp Tục </button>
                      </div>
                  </form>
            </div>
          </div>
        </div>
    </div>
        )
    }
}