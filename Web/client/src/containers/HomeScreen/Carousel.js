
import React from "react";
import { MDBCarousel, MDBCarouselInner, MDBCarouselItem, MDBMask } from
"mdbreact";
import '../../css/carousel.css'

const CarouselPage = () => {
  return (
    <div className="class">
        <MDBCarousel
            activeItem={1}
            length={4}
            showControls={true}
            showIndicators={true}
            className="z-depth-1"
        >
            <MDBCarouselInner>
                <MDBCarouselItem itemId="1">
                <div className="view-carousel" >
                    <img
                    className="d-block "
                    src="./../images/anhfilter4.jpg"
                    alt="First slide"
                    style={{width:1350 , height:400}}
                    />
                <MDBMask overlay="black-light" />
                </div>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="2">
                <div className="view-carousel-1">
                    <img
                    className="d-block "
                    src="../../images/anhfilter.png"
                    alt="Second slide"
                    style={{width:1350,height:400}}
                    />
                <MDBMask overlay="black-strong" />
                </div >
                </MDBCarouselItem>
                <MDBCarouselItem itemId="3">
                    <div className="view-carousel-2">
                    {/* background-image: linear-gradient(to right, purple, pink); */}
                        <img
                        className="d-block "
                        src="../../images/anhfilter1.jpg"
                        alt="Third slide"
                        style={{width:1350,height:400}}
                        />
                    <MDBMask overlay="black-slight" />
                    </div>
                </MDBCarouselItem>
                <MDBCarouselItem itemId="4">
                    <div className="view-carousel-3">
                        <img
                        className="d-block "
                        src="../../images/anhfilter5.png"
                        alt="Third slide"
                        style={{width:1350,height:400}}
                        />
                    <MDBMask overlay="black-slight" />
                    </div>
                </MDBCarouselItem>
            </MDBCarouselInner>
        </MDBCarousel>

    </div>
  );
}

export default CarouselPage;