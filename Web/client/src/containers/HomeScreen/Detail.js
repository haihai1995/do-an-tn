import React, { Component } from 'react';
import axios from 'axios';
import {Image} from 'react-bootstrap';
import Footer from '../Footer/Footer';
import CartContext from '../../context/Cart';
import {Link} from 'react-router-dom'
import Iframe from 'react-iframe';

export default class Edit extends Component {
    static contextType = CartContext;
    constructor(props) {
        super(props);

        this.state = {product: []};
        this.state = {
            name: '',
            status: '',
            check: '',
            price: '',
            image: '',
            detail: '',
            sale: '',
            promotionone: '',
            promotiontwo: '',
            memory: '',
            screen: '',
            pin: '',
            ram: '',
            rom: '',
            cpu: '',
            pk: '',
            guarantee: '',
            guarantees: '',
            image_src: '',

            nickname: '',
            phone: '',
            email:'',
            address:'',
            note:'',
            amount: '',

            id: ''
        }
    }

    onChangeNickname = (e) => {
        this.setState({
            nickname: e.target.value
        });
    }

    onChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        });
    }

    onChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        });
    }

    onChangeAddress = (e) => {
        this.setState({
            address: e.target.value
        });
    }

    onChangeNote = (e) => {
        this.setState({
            note: e.target.value
        });
    }

    onChangeAmount = (e) => {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeImages = (e) => {
        this.setState({
            id: e.target.value
        });
    }
    Onbuy = (product)=>{
        this.context.addProduct(product);
    }
    onSubmit = (e) => {
        e.preventDefault();

        const obj = {
            nickname: this.state.nickname,
            phone: this.state.phone,
            email: this.state.email,
            address: this.state.address,
            note: this.state.note,
            amount: this.state.amount,
            id: this.state.id
        };
        axios.post('http://localhost:4000/orders/add', obj)
            .then(res => console.log(res.data));

        this.setState({
            nickname: '',
            phone: '',
            email: '',
            address:'',
            note: '',
            amount: '',

            id: ''
        })
    }

    componentDidMount() {
        axios.get('http://localhost:5000/products/Detail/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                   
                    name: response.data.name,
                    status: response.data.status,
                    check: response.data.check,
                    price: response.data.price,
                    image: response.data.image,
                    sale: response.data.sale,
                    promotionone: response.data.promotionone,
                    promotiontwo: response.data.promotiontwo,
                    memory: response.data.memory,
                    screen: response.data.screen,
                    operating:response.data.operating,
                    pin: response.data.pin,
                    ram: response.data.ram,
                    rom: response.data.rom,
                    cpu: response.data.cpu,
                    pk: response.data.pk,
                    camera: response.data.camera,
                    camerax: response.data.camerax,
                    guarantee: response.data.guarantee,
                    guarantees: response.data.guarantees,
                    image_src: response.data.image_src,
                    detail: response.data.detail });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        return (
            <div className="container">
            <div className="row">
                <div className="col-10 mx-auto col-md-6 my-3 text-capitalize ">
                    <Image className="img-fluid" style={{width:450, marginLeft:100, marginTop:20}} alt="" src={this.state.image}/>
                </div>
                <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
                    <h2>{this.state.name}</h2><hr></hr>
                    <h4 className="text-blue">
                        <strong className="text-danger">
                        Giá: {this.state.price} <span>{this.state.currency}</span>
                        </strong>
                    </h4>
                    <div className="row text-center">
                        <div className="radio col-md-4 col-xs-6">
                            <input type="radio"/>&nbsp;<b>64GB</b>
                        </div>
                        <div className="col-md-4 col-xs-6">
                            <input type="radio"/>&nbsp;<b>128GB</b>
                        </div>
                        <div className="col-md-4 col-xs-6">
                            <input type="radio"/>&nbsp;<b>256GB</b>
                        </div>
                    </div>
                    <div className="sale mt-2">
                        <h5 className="mt-3 ml-4"><b> KHUYẾN MÃI </b></h5>
                            <p className="ml-4"> {this.state.sale} </p>
                        <p className="ml-4 text-danger">Phiếu mua hàng trị giá 1 triệu *</p>
                    </div>
                    <div className="mt-3">
                    <input type="radio"/>&nbsp;<b>Yêu cầu nhân viên kỹ thuật giao hàng:</b> 
                        <p>hỗ trợ copy danh bạ, hướng dẫn sử dụng máy mới, giải đáp thắc mắc sản phẩm.</p>
                    </div>
                    <div className="text-center">
                        <Link to ="/order" className="btn btn-warning" style={{width:170}}>MUA NGAY </Link>
                        <button className="btn btn-warning ml-4" style={{width:170}} onClick = {this.Onbuy}>GIỎ HÀNG </button>
                    </div>
                    <p className="text-secondary text-center mt-2">Gọi đặt mua: 1800.1060 (miễn phí - 7:30-22:00)</p>
                </div>
            </div>
            <div className="row">
                <div className="col-6">
                    <h2 className="text-capitalize font-weight-bold mb-0">
                        Đặc điểm nổi bật của iPhone 11 Pro Max 64GB:
                    </h2>
                    <p className="text-muted lead">{this.state.description}</p>
                    <p className="font-weight-bold">Hiệu năng "đè bẹp" mọi đối thủ</p>
                    <p>iPhone 11 Pro Max 512GB năm nay sử dụng chip Apple A13 Bionic mới nhất, nhanh và tiết kiệm điện hơn so với A12 năm ngoái.</p>
                    <Image className="img-fluid" src="../../images/iphonedetail.jpg"/>
                    <p>Máy cũng sở hữu riêng một con chip AI Neural Engine sẽ phụ trách các tính năng xử lý hình ảnh như Deep Fusion và Night Mode.</p>
                    <Image className="img-fluid" src="../../images/iphonedetail1.jpg"/>
                </div>
                <div className="col-6">
                    <h2 className="text-center font-weight-bold">Thông số kỹ thuật</h2><hr></hr>    
                        <p>Màn hình: {this.state.screen}</p><hr></hr>
                        <p>Hệ điều hành:	{this.state.operating}</p><hr></hr>
                        <p>Camera sau:	{this.state.camera}</p><hr></hr>
                        <p>Camera trước: {this.state.camerax}</p><hr></hr>
                        <p>CPU:	{this.state.cpu}</p><hr></hr>
                        <p>RAM:	{this.state.ram}</p><hr></hr>
                        <p>Bộ nhớ trong: {this.state.memory}</p><hr></hr>
                        <p>Dung lượng pin: {this.state.pin}</p><hr></hr>
                    <div className="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width=""></div>
                </div>
                <div>
										<Iframe className="iframe-fb" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=728127094590611"scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
									</div>
            </div>
     
        </div>

        )
    }
}