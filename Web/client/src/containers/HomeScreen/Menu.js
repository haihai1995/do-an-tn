import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { Image,Badge} from 'react-bootstrap';
import CartContext from '../../context/Cart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart, faAlignJustify } from '@fortawesome/free-solid-svg-icons'
import '../../css/index.css'


export default class Menu extends Component {
    constructor(props) {
      super(props);
      this.state = {
        value:'',
      }
    }
    // handleInputChange = (e) =>{
    //   this.state.valueSutmit(e.target.value);
    //   console.log(e.target.value);
      
    // }

    render(){
      return (
        <CartContext.Consumer>
            {({ count }) => (
              <div className="">
                <nav className="navbar navbar-expand-md navbar-light" style={{paddingTop:3,paddingBottom:3}}>
                  <div className="container">
                    <Link to="/" className="navbar-branch">
                      <Image src="../../images/logo.png" height={55} />
                    </Link>
                    <div className="dropdown">
                      <button className="" style={{height:60}}><FontAwesomeIcon icon={faAlignJustify} style={{width:100,height:40,color:"white",marginTop:4}}/></button>
                      <div className="dropdown-content">
                        <Link to="/apple"><Image src="../../images/logoiphone.jpg"></Image></Link>
                        <Link to="/samsung"><Image src="../../images/logosamsung.jpg"></Image></Link>
                        <Link to="/oppo"><Image src="../../images/logooppo.png"></Image></Link>
                        <Link to="/xiaomi"><Image src="../../images/logoxiaomi.jpg"></Image></Link>
                        <Link to=""><Image src="../../images/logovivo.jpg"></Image></Link>
                        <Link to=""><Image src="../../images/logovsmart.png"></Image></Link>
                      </div>
                    </div>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
                      <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                      
                      <form className="form-inline" >
                        <input className=" mr-sm-2"style={{backgroundColor:"white"}}
                            id="inputName"
                            placeholder="Search ..."
                            // value={this.state.query}
                            // onChange={this.handleInputChange}
                        />
                        <button className="btn btn-light my-sm-0" type="submit" style ={{backgroundColor:"orange"}}>Search</button>
                      </form>

                      <ul className="navbar-nav mx-auto">
                        <li className="nav-item" >
                        <CartContext.Consumer>
                            {({ count }) => (
                            <Link to="/cart" className="text-white">
                                &nbsp;&nbsp;<FontAwesomeIcon icon={faShoppingCart}/>&nbsp;
                                Giỏ Hàng
                                    <Badge style={{ marginLeft: 5, marginTop: 9  }} pill variant="primary">
                                    {count}
                                    </Badge>
                                </Link>
                            )}
                            </CartContext.Consumer>
                        </li>
                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
            )}            
        </CartContext.Consumer>
    );
}
}
