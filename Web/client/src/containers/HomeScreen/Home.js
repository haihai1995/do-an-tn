import React from 'react';
import axios from 'axios'
import ProductList from '../../components/Product/ProductList';
import CartContext from '../../context/Cart'
import Carousel from './Carousel'
// import Search from '../Search/Search'
import Filter from '../Filterhome/Filter';
import '../../css/Home.css'
// import {Link} from 'react-router-dom'



class HomeScreen extends React.Component {
  static contextType = CartContext;
constructor(props){
  super(props);
  this.state = {
    products: [],
    visible: 8,
  };
}
    componentDidMount() {
      axios.get(`http://localhost:5000/products`)
        .then(res => {
          this.setState({
              products: res.data
          })
      })
  }
  onBuy = (product) =>{
    this.context.addProduct(product);
  }

  handleClick= (_id) => {
    this.props.history.push(`/Detail/`+_id)
  };
  loadMore = () => {
    this.state.products.slice(0,this.state.visible).map(function(products){
      return products;
    })
      this.setState((prev) => {
          return {visible: prev.visible + 4};
      });
  }


  render() {
    const { products } = this.state;
    return (
      <div>
        <Carousel/>
        <Filter/>
        <div className="filter-container">
          <div className="container">
            <ProductList 
              list={products} 
              onBuy={this.onBuy}
              onItemClick={this.handleClick}
            />
            {/* <Delete/> */}
            {/* <Search/> */}
            <div className="text-center p-3">
              {this.state.visible < this.state.products.length &&
                <button onClick={this.loadMore} type="button" className="load-more btn btn-primary">Xem thêm</button>
              }
            </div>
          </div>
        </div>
        {/* <Search/> */}
      </div>
    );
  }
}

export default HomeScreen;
