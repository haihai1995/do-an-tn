import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Image } from 'react-bootstrap';
import '../../css/Filterhome.css'

export default class Filter extends Component {
  render() {
    return (
    <div className="shortcutsList">
      <div className="container"> 
        <div className="shortcutsList-list">
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter1.png" style={{width:70, height:70}}/></Link>
                <p>Flash sale Bách hóa</p>
            </div>
            <div className="image-filter">
                <Link to="/order"><Image src="../../images/filter2.gif" style={{width:70, height:70}}/></Link>
                <p>Giao hàng nhanh 3H</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter3.png" style={{width:70, height:70}}/></Link>
                <p>Tiêu dùng,mua bán điện thoại</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter4.png" style={{width:70, height:70}}/></Link>
                <p>Quyên góp chống COVID</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter5.png" style={{width:70, height:70}}/></Link>
                <p>Tiêu dùng hoàn 300k</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter6.png" style={{width:70, height:70}}/></Link>
                <p>Phiếu mua hàng shop</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter7.png" style={{width:70, height:70}}/></Link>
                <p>Nạp tiền điện thoại Mobile</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter8.png" style={{width:70, height:70}}/></Link>
                <p>Thanh toán hóa đơn</p>
            </div>
            <div className="image-filter">
                <Link to=""><Image src="../../images/filter10.png" style={{width:70, height:70}}/></Link>
                <p>Xu & Mã giảm giá shop</p>
            </div>
        </div>
      </div>
    </div>
    );
  }
}
