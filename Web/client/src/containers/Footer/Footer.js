import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faPhone, faEnvelope, faPrint} from '@fortawesome/free-solid-svg-icons'
import './Footer.css'
import { Image } from 'react-bootstrap';
class Footer extends Component {
    render() {
        return (
        <div className="bg-light">
          <footer className="page-footer font-small mdb-color lighten-3 pt-4">
          <div className="container text-center text-md-left">
            <div className="row">
              <div className="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">
                <h5 className="font-weight-bold text-uppercase mb-4">THÔNG TIN CỬA HÀNG</h5>
                <p>LFPT Retail đã phát triển hệ thống chuỗi cửa hàng rộng 
                  khắp trên toàn quốc ở 63 tỉnh thành</p>
                <p>tạo dựng được niềm tin nơi Quý khách hàng khi là nhà bán lẻ đứng thứ 1 về thị phần máy 
                  tính xách tay tại Việt Nam, đứng thứ 2 về thị phần điện thoại và là 
                  nhà bán lẻ Apple chính hãng hàng đầu tại 
                  Việt Nam</p>
              </div>
              <hr className="clearfix w-100 d-md-none" />
              <div className="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">
                <h5 className="font-weight-bold text-uppercase mb-4">HỖ TRỢ KHÁCH HÀNG</h5>
                <ul className="list-unstyled">
                  <li>
                    <p>
                      <a href="#!">Tìm Hiểu về trả góp</a>
                    </p>
                  </li>
                  <li>
                    <p>
                      <a href="#!">Giao hàng , lắp đặt</a>
                    </p>
                  </li>
                  <li>
                    <p>
                      <a href="#!">Bảo hành , đổi trả</a>
                    </p>
                  </li>
                  <li>
                    <p>
                      <a href="#!">Chất lượng phục vụ</a>
                    </p>
                  </li>
                  <li>
                    <p>
                      <a href="#!">Hướng đẫn mua hàng</a>
                    </p>
                  </li>
                </ul>
              </div>
              <hr className="clearfix w-100 d-md-none" />
              <div className="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">
                <h5 className="font-weight-bold text-uppercase mb-4">LIÊN HỆ</h5>
                <ul className="list-unstyled">
                  <li>
                    <p>
                      <FontAwesomeIcon icon={faHome}/> New York, NY 10012, US</p>
                  </li>
                  <li>
                    <p>
                      <FontAwesomeIcon icon={faEnvelope}/> info@example.com</p>
                  </li>
                  <li>
                    <p>
                      <FontAwesomeIcon icon={faPhone}/> + 01 234 567 88</p>
                  </li>
                  <li>
                    <p>
                      <FontAwesomeIcon icon={faPrint}/> + 01 234 567 89</p>
                  </li>
                </ul>
              </div>
              <hr className="clearfix w-100 d-md-none" />
            
              <div className="col-md-2 col-lg-2 text-center mx-auto my-4">
              
                <h5 className="font-weight-bold text-uppercase mb-4">Xã Hội</h5>
          
                <a type="" className="btn-floating btn-fb">
                  <h5>Website cùng tập đoàn:</h5>
                </a>
          
                <a type="" className="btn-floating btn-tw">
                    <Image src="../../../images/face.png"/>
                </a>
                <a type="" className="btn-floating btn-gplus">
                  <Image src="../../../images/youtobe.jpg"/>
                </a>
                <a type="" className="btn-floating btn-dribbble">
                  <Image src="../../../images/Twitter.jpg"/>
                </a>
              </div>
            </div>
          </div>
          <div className="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
          </div>
        </footer>
      </div>
        );
    }
}

export default Footer;