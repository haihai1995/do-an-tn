import React from 'react';
import CartContext from '../../context/Cart';
import CartList from '../../components/Cart/PayPalButton';
import CartTotal from '../../components/Cart/CartTotal';
import '../../css/Cart.css';
import axios from 'axios'
class CartScreen extends React.Component {
  static contextType = CartContext;
  state = {
    products: [],
    idAlert: '',
    count:"",
    newProduct: [],

  };

  render() {
    const {product} = this.state
    return (
    <div className="back-cart">
      <div className="container cart-screen">
        <CartContext.Consumer>
          {({ cart, count, getTotal }) => {
            return (
            <div>
              <div className="d-flex cart-wrapper">
                <div style={{ flex: 1 }}>
                  <CartList list={cart}
                  />
                </div>
              </div>
                <div className="" style={{ marginLeft: 15 }}>
                  <CartTotal total={getTotal()} 
                  />
                </div>
              </div>
            );
          }}
        </CartContext.Consumer>
      </div>
    </div>
    );
  }
}
export default CartScreen;