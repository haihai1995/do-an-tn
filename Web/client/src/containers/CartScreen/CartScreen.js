import React from 'react';
import CartContext from '../../context/Cart';
import CartList from '../../components/Cart/CartList';
import CartTotal from '../../components/Cart/CartTotal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import '../../css/Cart.css';
import axios from 'axios'
class CartScreen extends React.Component {
  static contextType = CartContext;
  state = {
    products: [],
    idAlert: '',
    count:"",
    newProduct: [],

  };

  

  onDelete = (product) =>{
    // let products = this.state.products;
    // products.splice(product,1);
    // this.setState({
    //   products
    // })
    console.log(product);
    
    this.context.handleDelete(product);
  }

  onSubmit =(newProduct)=>{
        const obj = {
            name: this.state.name,
            price: this.state.price,
            province: this.state.province,
            ward: this.state.ward,
            address: this.state.address
        };
        axios.post('http://localhost:5000/order', obj)
            .then(res => console.log(res.data));
        this.setState({
            name: '',
            price: '',
            province: '',
            ward:'',
            address:'',
        })
    }
  

  increment = (product) =>{
    this.context.addProduct(product);
  }
  
  decrement = (product) =>{
    this.context.decrement(product)
  }
  render() {
    const {product} = this.state
    return (
    <div className="back-cart">
      <div className="container cart-screen">
        <div className="container" style={{marginLeft:-7,borderRadius:2}}>
            <div className="list-item-1" style={{backgroundColor: "white"}}>
              <nav className="checkout-item">
                <ul className="breadcrumb"style={{backgroundColor:"white", fontSize:10, color:"darkgray"}}>
                  <li className="item_1wt2">Shopping&nbsp;</li>
                  <li className="item_1wt3"><FontAwesomeIcon icon={faChevronRight}/>&nbsp;Giỏ Hàng&nbsp;</li>
                  <li className="item_1wt4"><FontAwesomeIcon icon={faChevronRight}/>&nbsp;Thanh Toán</li>
                </ul>
                <h4 className="" style={{marginLeft:15, fontSize:15}}>GIỎ HÀNG CỦA BẠN</h4>
              </nav>
            </div>
          </div>
        <CartContext.Consumer>
          {({ cart, count, getTotal }) => {
            if (count === 0) {
              return (
                <div className="text-center">
                  <h1 className="text-danger">Không có sản phẩm nào trong giỏ hàng.</h1>
                </div>
              );
            }
            return (
            <div>
              <div className="d-flex cart-wrapper">
                <div style={{ flex: 1 }}>
                  <CartList list={cart}
                    removeItem={this.onDelete}
                    increment = {this.increment}
                    decrement = {this.decrement}
                  />
                </div>
              </div>
                <div className="" style={{ marginLeft: 15 }}>
                  <CartTotal total={getTotal()} 
                    onSummitChange={this.onSubmit}
                  />
                </div>
              </div>
            );
          }}
        </CartContext.Consumer>
      </div>
    </div>
    );
  }
}
export default CartScreen;