import React, { Component } from 'react';
import '../css/productItem.css';
import { Button, Card, Image } from 'react-bootstrap';
import Ranting from '../components/Product/Ranting';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'
import Carousel from '../containers/HomeScreen/Carousel'

export default class oppo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: [],
            visible: 8,
            error: false,
            query: "oppo",
            filteredData: [],
        };
    };

    loadMore = () => {
        this.setState((prev) => {
            return { visible: prev.visible + 4 };
        });
    }

    getData = () => {
        fetch(`http://localhost:5000/products`)
            .then(response => response.json())
            .then(persons => {
                const { query } = this.state;
                const filteredData = persons.filter(element => {
                    return element.manufacturer.toLowerCase().includes(query.toLowerCase());
                });

                this.setState({
                    persons,
                    filteredData
                });
            });
    };

    componentWillMount() {
        this.getData();
    }

    render() {
        return (
            <div>
            <Carousel/>
                <div className="container">
                    <div className="" style={{marginTop:10, marginLeft:15}}>Tìm thấy 7 kết quả.</div>
                    <div className="d-flex flex-wrap">
                        {this.state.filteredData.slice(0, this.state.visible).map((i, index) =>
                        <div className="col-md-3 col-xs-6">
                            <div className="mt-4">
                                <Card className="cart">
                                    <div className="img-container p-5">
                                    <Image className="card-img-top" src={i.image}  />   
                                    <Button className="cart-btn" variant="danger"><FontAwesomeIcon icon={faCartPlus}/></Button>
                                    </div>           
                                    <Card.Body className="card-footer justify-content-between">
                                        <div >
                                        <Card.Title>
                                        <p style={{cursor: 'pointer'}}>
                                                {i.name}
                                            </p>
                                        </Card.Title>
                                        <Card.Text className="text-danger">
                                            {i.price}&nbsp;{i.currency}
                                        </Card.Text>
                                        <Card.Text className="">
                                            {i.status}
                                        </Card.Text>
                                        </div>
                                        <Ranting/>
                                    </Card.Body>
                                </Card>
                            </div>
                        </div>
                        )}
                    </div>
                    <div className="text-center p-3">
                        {this.state.visible < this.state.persons.length &&
                            <button onClick={this.loadMore} type="button" className="load-more btn btn-success dropdown-toggle">See more </button>
                        }
                    </div>
                </div>
            </div>
        )
    }
}
