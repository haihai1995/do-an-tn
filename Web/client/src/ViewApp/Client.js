import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { CartProvider } from '../context/Cart';
import Home from '../containers/HomeScreen/Home';
import Login from '../components/SignIn/Login'
import Register from '../components/SignIn/Register'
import Profile from '../components/SignIn/Profile'
import Header from '../components/Header/Header'
import CartScreen from '../containers/CartScreen/CartScreen'
import Menu from '../containers/HomeScreen/Menu'
import Detail from '../containers/HomeScreen/Detail'
import Order from '../containers/ Order/order'
import Footer from '../containers/Footer/Footer';
import SliderFilter from '../containers/Filterhome/SliderFilter';
import Backtop from '../controls/Backtop'

class App extends Component {
  render() {
    return (
    <div>
      <CartProvider>
        <Router>
          <div className="">
            <Backtop/>
            <div className="">
              <Header/>
              <Menu/>
              <Route path="/" exact component={Home} />
              <Route path="/cart" exact component={CartScreen} />
              <Route path="/Detail/:id" exact component={Detail} />
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <Route path="/profile" component={Profile} />
              <Route path="/order" component={Order} />
              <SliderFilter/>
              <Footer/>
            </div>
          </div>
        </Router>
      </CartProvider>
    </div>
    )
  }
}

export default App;