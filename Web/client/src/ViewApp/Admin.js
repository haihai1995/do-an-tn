import React, { Component } from 'react'
import { BrowserRouter as Router, Route , Redirect, Switch} from 'react-router-dom'
import { CartProvider } from '../context/Cart';
// import Login from '../ViewAdmin/LoginAdmin/SignIn';
// import Admin from '../ViewAdmin/Adminfilter/Admin';
// import Logout from '../ViewAdmin/LoginAdmin/Logout';

import Admin from "../ViewAdmin/layouts/Admin";
import RTL from "../ViewAdmin/layouts/RTL";
import LoginAdmin from "../ViewAdmin/layouts/LoginAdmin";
import { createBrowserHistory } from "history";
import "../ViewAdmin/assets/css/dashboard-react.css";

const hist = createBrowserHistory();

class App extends Component {
  
  render() {
    return (
    <div>
      <CartProvider>
        <Switch>
          <Router history = {hist}>
            <div>
              <Route exact path = "/LoginAdmin" component ={LoginAdmin}/>
              <Route path="/admin" component={Admin} />
              <Route path="/rtl" component={RTL} />
              <Redirect from="/" to="/LoginAdmin" />
            </div>
          </Router>
        </Switch>
      </CartProvider>
    </div>
    )
  }
}

export default App;