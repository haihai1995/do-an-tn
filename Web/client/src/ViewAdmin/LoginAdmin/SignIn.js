// import React, { Component } from 'react';
// import {Link} from 'react-router-dom'
// import {Redirect } from 'react-router-dom'

// const formValid = ({ formErrors, ...rest }) => {
//   let valid = true;

//   // validate form errors being empty
//   Object.values(formErrors).forEach(val => {
//     val.length > 0 && (valid = false);
//   });

//   // validate the form was filled out
//   Object.values(rest).forEach(val => {
//     val === null && (valid = false);
//   });

//   return valid;
// };

// class SignIn extends Component {
//   constructor(props) {
//     super(props);
//     let loggedIn = false
//     this.state={
//       username:'',
//       password:'',
//       loggedIn,
//       formErrors :{
//         username:'',
//         password:""
//       }
//     }
//   }

//   onChange = (e) =>{
//     this.setState({
//       [e.target.name] : e.target.value
//     })
//     const {name , value} = e.target;
//     let formErrors = {...this.state.formErrors};
//     switch(name) {
//       case "username":
//         formErrors.username = 
//           value.length < 3 ? "minimum 3 characaters required" : "";
//           break;
//       case "password":
//         formErrors.password = 
//         value.length < 6 ? "minimum 6 characaters required" : "";
//         break;
//         default:
//         break;
//     }

//     this.setState({formErrors , [name]: value}, ()=> console.log(this.state));
//   }
//   submitForm = (e) =>{
//     e.preventDefault()
//     const {username , password} = this.state
//      if(username ==="A", password==="a"){
//        localStorage.setItem("token" , "asasd")
//        this.setState({
//          loggedIn : true
//        })
//      }
//   }

//   render() {
//     const {formErrors} = this.state;
//     if(this.state.loggedIn){
//       return <Redirect to ="/Api/Admin"/>
//     }
//     return (
//       <div> 
//         <form onSubmit = {this.submitForm} noValidate>
//           <input type="text"
//             className = {formErrors.username.length > 0 ? "error" : null}
//             placeholder="username" 
//             name="username" 
//             noValidate
//             onChange={this.onChange}
//           />
//           { formErrors.username.length >0 && (
//             <span className="errorMessage">{formErrors.username}</span>
//           )}
//           <input
//             className={formErrors.password.length > 0 ? "error" : null}
//             placeholder="Password"
//             type="password"
//             name="password"
//             noValidate
//             onChange={this.onChange}
//           />
//           {formErrors.password.length > 0 && (
//                 <span className="errorMessage">{formErrors.password}</span>
//               )}
//           <input type="submit"/>
//         </form>
//       </div>
//     );
//   }
// }

// export default SignIn;


import React, { Component } from 'react'
import { login } from '../../components/SignIn/UserFunctions'
import '../../css/login.css'
import axios from 'axios';


const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

class Login extends Component {
  constructor() {
    super()
    this.state = {
      username: '',
      password: '',
      formErrors: {
        username: "",
        password: ""
      }
    }

  }

  onChange =(e)=> {
    this.setState({ [e.target.name]: e.target.value })
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };
    switch (name) {
      case "username":
        formErrors.firstName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "password":
        formErrors.password =
          value.length < 6 ? "minimum 6 characaters required" : "";
        break;
      default:
        break;
    }

    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };
  
  onSubmit =(e)=> {
    e.preventDefault()
    const user = {
      username: this.state.username,
      password: this.state.password
    };
    axios.get('http://localhost:5000/loginadmin', user)
      .then(res => console.log(res.data));
      this.setState({
        username:'',
        password:''
      })
    
    login(user).then(res => {
      if (res) {
        this.props.history.push(`/api/admin`)
      }
    })
  }

  render() {
    const { formErrors } = this.state;
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h1>Create Account</h1>
          <form onSubmit={this.onSubmit} noValidate>
            <div className="username">
              <label htmlFor="username">username</label>
              <input
                 className={formErrors.username.length > 0 ? "error" : null}
                placeholder="username"
                type="username"
                name="username"
                noValidate
                onChange={this.onChange}
              />
              {formErrors.username.length > 0 && (
                <span className="errorMessage">{formErrors.username}</span>
              )}
            </div>
            <div className="password">
              <label htmlFor="password">Password</label>
              <input
                className={formErrors.password.length > 0 ? "error" : null}
                placeholder="Password"
                type="password"
                name="password"
                noValidate
                onChange={this.onChange}
              />
              {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
            </div>
            <div className="createAccount">
              <button type="submit">Create Account</button>
              <small>Already Have an Account?</small>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default Login

