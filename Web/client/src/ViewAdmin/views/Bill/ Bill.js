
import React, { Component } from 'react';
import axios from 'axios'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import PrintIcon from '@material-ui/icons/Print';
import Snackbar from '@material-ui/core/Snackbar';
import Tooltip from '@material-ui/core/Tooltip';
import '../../assets/css/dashboard-react.css'
import { Link } from "react-router-dom";
import DeleteIcon from '@material-ui/icons/Delete';


export default class UserProfile extends Component {
  constructor(props){
    super(props);
    this.state = {
      persons: [],
      visible:8,
      error: null,
      isLoading:true,
      snackbaropen:false, 
      snackbarmsg:''
    }
}
  snackbarClose = (event) =>{
    this.setState({snackbaropen:false});
  }
  getPosts(){
    axios.get('http://localhost:5000/order/')
    .then(response => {
        this.setState({persons: response.data,
          isLoading: false
        });
    })
    .catch(error => this.setState({ error, isLoading:false}));
}
componentDidMount(){
 
  this.getPosts();
}

// onDelete = userId => {
//   const persons = this.state.persons.filter(item => item._id !== userId);
//   this.setState({persons: persons})
//   this.setState({snackbaropen:true, snackbarmsg:'Email deleted successfully!'})
// }


onDelete = (_id) => {
  this.props.history.push(`/admin/Orderanim/`+_id)
}

  render() {
    const {isLoading , persons} = this.state;
    return (
      <div className="row">
          <TableContainer >
          <Table aria-label="caption table">
            <caption>A basic table example with a caption</caption>
            <TableHead>
              <TableRow style={{backgroundColor:'black'}}>
                <TableCell style={{color:'white'}} align="center">STT</TableCell>
                <TableCell style={{color:'white'}} align="center">Mã đơn hàng</TableCell>
                <TableCell style={{color:'white'}} align="center">Thời gian </TableCell>
                <TableCell style={{color:'white'}} align="center">Tình trang đơn hàng </TableCell>
                <TableCell style={{color:'white'}} align="center">Print</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                this.state.persons.map((user, index)=>{
                  return <TableRow key={user._id}>
                  <TableCell component="th" scope="row"align="center">
                    {index +1}
                  </TableCell>
                  <TableCell align="center">{user._id}</TableCell>
                  <TableCell align="center">{user.date}</TableCell>
                  <TableCell align="center">Đang sử lý</TableCell>
                  <TableCell align="center">
                  <Tooltip title="Print">
                  {/* <IconButton aria-label="print">
                      <Link to={"/admin/Orderanim" + user._id}><PrintIcon /></Link>
                    </IconButton> */}
                      <IconButton aria-label="delete"onClick = {()=>{this.onDelete (user._id)}}>
                      <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                  </TableCell>
                </TableRow>
                })
              }
            </TableBody>
          </Table>
        </TableContainer>
        <Snackbar
            anchorOrigin={{vertical:'right', horizontal:'right'}}
            open = {this.state.snackbaropen}
            autoHideDuration = {3000}
            onClose = {this.snackbarClose}
            message = {<span id ="message-id">{ this.state.snackbarmsg}</span>}
            action= {[
              <IconButton 
                key="close"
                arial-label ="Close"
                color = "inherit"
                onClick={this.snackbarClose }
              >
                xGiá:
              </IconButton>
              
            ]}
          />
      </div>
    )
  }
}

