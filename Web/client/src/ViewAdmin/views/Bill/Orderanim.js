
import React, { Component } from 'react';
import axios from 'axios'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Snackbar from '@material-ui/core/Snackbar';
import Tooltip from '@material-ui/core/Tooltip';
import '../../assets/css/dashboard-react.css'


export default class UserProfile extends Component {
  constructor(props){
    super(props);
    this.state = {
      persons: [],
      visible:8,
      error: null,
      isLoading:true,
      snackbaropen:false, 
      snackbarmsg:''
    }
}
  snackbarClose = (event) =>{
    this.setState({snackbaropen:false});
  }
  getPosts(_id){
    axios.get('http://localhost:5000/order/')
    .then(response => {
        this.setState({persons: response.data,
          isLoading: false
        });
    })
    .catch(error => this.setState({ error, isLoading:false}));
}
componentDidMount(){
 
  this.getPosts();
}


onDelete = (_id) => {
  this.props.history.push(`/admin/Orderanim/`+_id)
}

  render() {
    const {isLoading , persons} = this.state;
    return (
    <div>
        <div className="col-10 mx-auto col-md-6 my-3 text-capitalize">
          <h2>{this.state.name}</h2><hr></hr>
          <h4 className="text-blue">
            <strong className="text-danger">
              Giá: {this.state.phone} <span>{this.state.currency}</span>
            </strong>
           </h4>
        </div>
    </div>
    )
  }
}
