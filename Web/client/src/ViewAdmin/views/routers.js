// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt';
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import PhonelinkRingIcon from '@material-ui/icons/PhonelinkRing';
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
// core components/views for Admin layout
import DashboardPage from "./Dashboard/Dashboard";
import UserProfile from "./UserProfile/UserProfile.js";
import TableList from "./TableList/TableList.js";
import Typography from "./Typography/Typography.js";
import Icons from "./Icons/Icons.js";
import Maps from "./Maps/Maps.js";
import NotificationsPage from "./Notifications/Notifications.js";
import UpgradeToPro from "./UpgradeToPro/UpgradeToPro.js";
import Bill from "./Bill/ Bill";
import Orderanim from './Bill/Orderanim';
// core components/views for RTL layout
import RTLPage from "./RTLPage/RTLPage.js";
import './Bill/style.scss';

const dashboardRoutes = [
  {
    redirect: true,
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "Thông Tin Người Dùng",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/table",
    name: "Sản Phẩm Đang Bán",
    rtlName: "قائمة الجدول",
    icon: PhonelinkRingIcon,
    component: TableList,
    layout: "/admin"
  },
  {
    path: "/typography",
    name: "Thêm Sản Phẩm",
    rtlName: "طباعة",
    icon: SystemUpdateAltIcon,
    component: Typography,
    layout: "/admin"
  },
  {
    path: "/bill",
    name: "Quản lý hóa đơn",
    rtlName: "ملف تعريفي للمستخدم",
    icon: LibraryBooksIcon,
    component: Bill,
    layout: "/admin",
  },
  {
    path: "/Orderanim",
    name: "In hóa đơn",
    rtlName: "ملف تعريفي للمستخدم",
    icon: LibraryBooksIcon,
    component: Orderanim,
    layout: "/admin",
  },
  {
    path: "/icons",
    name: "Icons",
    rtlName: "الرموز",
    icon: BubbleChart,
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    rtlName: "خرائط",
    icon: LocationOn,
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    rtlName: "إخطارات",
    icon: Notifications,
    component: NotificationsPage,
    layout: "/admin"
  },
  {
    path: "/rtl-page",
    name: "RTL Support",
    rtlName: "پشتیبانی از راست به چپ",
    icon: Language,
    component: RTLPage,
    layout: "/rtl"
  },
  {
    path: "/upgrade-to-pro",
    name: "Upgrade To PRO",
    rtlName: "التطور للاحترافية",
    icon: Unarchive,
    component: UpgradeToPro,
    layout: "/admin"
  }
];

export default dashboardRoutes;
