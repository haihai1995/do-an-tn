// import React, { Component } from 'react';
// import TextField from '@material-ui/core/TextField';
// import '../../assets/css/dashboard-react.css'
// import axios from 'axios';
// import PropTypes from 'prop-types';
// import TextareaAutosize from '@material-ui/core/TextareaAutosize';
// import { InputNumber } from 'antd';

// export default class Typography extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//     product: [],
//     file: '',
//     imagePreviewUrl: '',
//     name: '',
//     status: '',
//     check: '',
//     price: '',
//     image: '',
//     description:'',
//     detail: '',
//     sale: '',
//     promotionone: '',
//     promotiontwo: '',
//     manufacturer: '',
//     screen: '',
//     pin: '',
//     ram: '',
//     rom: '',
//     cpu: '',
//     pk: '',
//     guarantee: '',
//     guarantees: '',
//     image_src: ''
//   };
// }


//  onSubmit =(e) => {
//    e.preventDefault();
//    const obj ={
//      name: this.state.name,
//      status: this.state.status,
//      image: this.state.image,
//      price: this.state.price,
//      sale: this.state.sale,
//      description: this.state.description,

//    };
//    axios.post('http://localhost:5000/products', obj)
//     .then(res => console.log(res.data));
//     this.setState({
//       name: '',
//       status: '',
//       price: '',
//       image: '',
//       description:'',
//       detail: '',
//       sale: '',
//     })
//  }

//  onChangeName = (e) =>{
//   this.setState({
//     name: e.target.value
//   });
//  }
//  onChangePrice = (e)=>{
//    console.log(e)
//  }
//   _handleImageChange(e) {
//     e.preventDefault();

//     let reader = new FileReader();
//     let file = e.target.files[0];

//     reader.onloadend = () => {
//       this.setState({
//         file: file,
//         imagePreviewUrl: reader.result
//       });
//     }

//     reader.readAsDataURL(file)
//   }
//   render() {
//     let {imagePreviewUrl} = this.state;
//     let $imagePreview = null;
//     if (imagePreviewUrl) {
//       $imagePreview = (<img src={this.state.image} />);
//     } else {
//       $imagePreview = (<div className="previewText" style={{width:600, height:600}}>
//       Vui lòng chọn một hình ảnh để xem trước</div>);
//     }
//     return (
//       <div className="container">
//         <form onSubmit={this.onSubmit} className="form-list">
//         <div>
//         <TextField style={{width: 1110}}
//           id="standard-textarea"
//           label="Tên Sản Phẩm"
//           placeholder="Placeholder"
//           value ={this.state.name}
//           multiline
//           onChange={this.onChangeName}
//         />
//         </div>
//         <div>
//         <TextField style={{marginTop:20, width: 1110}}
//           id="standard-textarea"
//           label="Giá Sản Phẩm"
//           placeholder="Placeholder"
//           value ={this.state.price}
//           multiline
//           onChange={this.onChangePrice}
//         />
//         <TextField style={{marginTop:20, width: 1110}}
//           id="standard-textarea"
//           label="Số lượng bán"
//           placeholder="Placeholder"
//           value ={this.state.price}
//           multiline
//           onChange={this.onChangePrice}
//         />
//         </div>
//         <div className="previewComponent" style={{marginTop:20}}>
//           <form onSubmit={(e)=>this._handleSubmit(e)}>
//             <input className="fileInput" 
//               type="file" 
//               onChange={(e)=>this._handleImageChange(e)} />
//           </form>
//           <div className="imgPreview"style={{textAlign:"center", marginTop: 30}}>
//             {$imagePreview}
//           </div>
//         </div>
//         <div>
//           <h1>dí</h1>
//         <TextareaAutosize
//           className="emnity"
//           rowsMax={10}
//           aria-label="maximum height"
//           placeholder="Maximum 4 rows"
//           defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
//               ut labore et dolore magna aliqua."
//         />
//         </div>
//         <div>
//         <form  noValidate>
//           <TextField style={{marginTop:20}}
//             id="datetime-local"
//             label="Date update"
//             type="datetime-local"
//             defaultValue="2020-05-24T10:30"
//             className=""
//             InputLabelProps={{
//               shrink: true,
//             }}
//           />
//         </form>
//         </div>
//         <div className="form-custom">
//           <input type="submit" value="Tiếp tục" className="btn btn-primary" style={{backgroundColor:"red"}}/>
//         </div>
//         </form>
//       </div>
//     );
//   }
// }


import React, { Component } from 'react';
import ImageUploader from "react-images-upload";
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import '../../assets/css/dashboard-react.css'

export default class  extends Component {
  constructor(props) {
    super(props);
    this.state = {
       pictures: [] ,
      file: '',
      imagePreviewUrl: '',
      name: '',
      status: '',
      check: '',
      price: '',
      image: '',
      description:'',
      detail: '',
      sale: '',
      promotionone: '',
      promotiontwo: '',
      manufacturer: '',
      screen: '',
      pin: '',
      ram: '',
      rom: '',
      cpu: '',
      pk: '',
      guarantee: '',
      guarantees: '',
      image_src: '',
      amounts :''
    };
    this.onDrop = this.onDrop.bind(this);
  }

  onChangeName = (e) =>{
      this.setState({
        name: e.target.value
      });
    }
  onChangePrice = (e)=>{
      this.setState({
        price: e.target.value
      });
    }
    onChangeAmounts = (e) =>{
      this.setState({
        amounts: e.target.value
      });
    }

  onSubmit =(e) => {
       e.preventDefault();
       const obj ={
         name: this.state.name,
         status: this.state.status,
         image: this.state.image,
         price: this.state.price,
         sale: this.state.sale,
         description: this.state.description,
         status: this.state.status
       };
       axios.post('http://localhost:5000/products', obj)
        .then(res => console.log(res.data));
        this.setState({
          name: '',
          status: '',
          price: '',
          image: '',
          description:'',
          detail: '',
          sale: '',
          amounts:''
        })
     }
    
  onDrop(pictureFiles, pictureDataURLs) {
    this.setState({
      pictures: this.state.pictures.concat(pictureFiles)
    });
  }

  render() {
    return (
      <div className="container">
        <form onSubmit={this.onSubmit} className="form-list">
          <div>
            <TextField style={{width: 1110}}
              id="standard-textarea"
              label="Tên Sản Phẩm"
              placeholder="Placeholder"
              value ={this.state.name}
              multiline
              onChange={this.onChangeName}
            />
          </div>
          <div>
          <TextField style={{marginTop:20, width: 1110}}
              id="standard-textarea"
              label="Giá Sản Phẩm"
              placeholder="Gía bán"
              value ={this.state.price}
              multiline
              onChange={this.onChangePrice}
            />
          </div>
          <div>
          <TextField style={{marginTop:20, width: 1110}}
              id="standard-textarea"
              label="Số lượng bán"
              placeholder="Số lượng"
              value ={this.state.amounts}
              multiline
              onChange={this.onChangeAmounts}
            />
          </div>
          <div className="App" style={{marginTop:20}}>
            <div
              style={{
                display: "flex",
                border: "1px solid red"
              }}
            >
              <div style={{ marginRight: "15px" }}>
                <ImageUploader
                  withIcon={false}
                  withPreview={true}
                  label=""
                  buttonText="Upload Images"
                  onChange={this.onDrop}
                  imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                  maxFileSize={1048576}
                  fileSizeError=" file size is too big"
                />
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    textAlign: "left",
                    marginBottom: "10px"
                  }}
                >
                  <label>Start Date</label>
                  <input type="date" />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    textAlign: "left"
                  }}
                >
                  <label>Start Date</label>
                  <input type="date" />
                </div>
              </div>
            </div>
          </div>
           <div className="form-custom">
           <button onClick={this.onSubmit} value="Tiếp tục" className="btn btn-primary" style={{backgroundColor:"red"}}> Tiếp Tục </button>
           </div>
          </form>
        </div>
    );
  }
}

