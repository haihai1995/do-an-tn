
import React, { Component } from 'react';
import axios from 'axios'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Snackbar from '@material-ui/core/Snackbar';
import '../../assets/css/dashboard-react.css';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';


export default class UserProfile extends Component {
  constructor(props){
    super(props);
    this.state = {
      products: [],
      visible:8,
      error: null,
      isLoading:true,
      snackbaropen:false, 
      snackbarmsg:''
    }
}
  snackbarClose = (event) =>{
    this.setState({snackbaropen:false});
  }
  getPosts(){
    axios.get('http://localhost:5000/products/')
    .then(response => {
        this.setState({products: response.data,
          isLoading: false
        });
    })
    .catch(error => this.setState({ error, isLoading:false}));
}
componentDidMount(){
 
  this.getPosts();
}

onDelete = userId => {
  const products = this.state.products.filter(item => item._id !== userId);
  this.setState({products: products})
  this.setState({snackbaropen:true, snackbarmsg:'Successfully deleted the product!'})
}

  render() {
    return (
      <div className="row">

          <TableContainer >
          <Table aria-label="caption table">
            <caption>A basic table example with a caption</caption>
            <TableHead>
              <TableRow style={{backgroundColor:'black'}}>
                <TableCell style={{color:'white'}} align="">STT</TableCell>
                <TableCell style={{color:'white'}} align="center">Hình ảnh</TableCell>
                <TableCell style={{color:'white'}} align="center">Tên Sản Phẩm</TableCell>
                <TableCell style={{color:'white'}} align="center">Giá Tiền</TableCell>
                <TableCell style={{color:'white'}} align="center">Số lượng bán</TableCell>
                <TableCell style={{color:'white'}} align="center">Thời gian Thêm Sản phẩm</TableCell>
                <TableCell style={{color:'white'}} align="center">Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {
                this.state.products.map((user, index)=>{
                  return <TableRow key={user._id}>
                  <TableCell component="th" scope="row"align="center">
                    {index +1}
                  </TableCell>
                  <TableCell align="center" >
                    <Avatar alt="Remy Sharp" src={user.image} />
                  </TableCell>
                  <TableCell align="center">{user.name}</TableCell>
                  <TableCell align="center">{user.price}&ensp;{user.currency}</TableCell>
                  <TableCell align="center">{user.amounts}</TableCell>
                  <TableCell align="center">{user.date}</TableCell>
                  <TableCell align="center">
                    <Tooltip title="Delete">
                        <IconButton aria-label="delete"onClick = {()=>{this.onDelete (user._id)}}>
                        <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                  </TableCell>
                </TableRow>
                })
              }
            </TableBody>
          </Table>
        </TableContainer>
        <Snackbar
            anchorOrigin={{vertical:'right', horizontal:'right'}}
            open = {this.state.snackbaropen}
            autoHideDuration = {3000}
            onClose = {this.snackbarClose}
            message = {<span id ="message-id">{ this.state.snackbarmsg}</span>}
            action= {[
              <IconButton 
                key="close"
                arial-label ="Close"
                color = "inherit"
                onClick={this.snackbarClose }
              >
                x
              </IconButton>
              
            ]}
          />
      </div>
    )
  }
}
