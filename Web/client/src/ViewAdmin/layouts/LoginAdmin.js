import React, { Component } from 'react'
import { login } from '../../components/SignIn/UserFunctions'
import Typography from '@material-ui/core/Typography';
import '../assets/css/dashboard-react.css'
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import axios from 'axios';
import {Redirect} from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';
import { loginadmin } from './Adminfunction'


const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);
const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

class Login extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      loggedIn:'',
      formErrors: {
        email: "",
        password: ""
      }
    }
  }

  onChange =(e)=> {
    this.setState({ [e.target.name]: e.target.value })
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };
    switch (name) {
      case "firstName":
        formErrors.firstName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "lastName":
        formErrors.lastName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "email":
        formErrors.email = emailRegex.test(value)
          ? ""
          : "invalid email address";
        break;
      case "password":
        formErrors.password =
          value.length < 6 ? "minimum 6 characaters required" : "";
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };
  
  onSubmit =(e)=> {
    e.preventDefault()
    const {email,password} = this.state
    const user = {
      email: this.state.email,
      password: this.state.password
    }
  //   axios.get('http://localhost:5000/loginadmin', user)
  //     .then(res => console.log(res.data));
  //     this.setState({
  //       email:'',
  //       password:''
  //     })

    if(email === "tranvanhai100195@gmail.com" && password ==="123456"){
      console.log("ddawng nhap thanh cong");
      
      this.setState({
        loggedIn: true
      })
    }else{
      return(
      <Alert severity="error">This is an error alert — check it out!</Alert>
      )
    }

  //   login(user).then(res => {
  //     if (res) {
  //       this.props.history.push(`/admin/`)
  //     }
  //   })
   }


  render() {
    const { formErrors } = this.state;
    if(this.state.loggedIn){
      return <Redirect to ="/admin"/>
      
    }
    return (
      <div className="wrapper-back">
        <div className="form-wrapper">
        <Avatar>
          <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
        </Typography>
          <form onSubmit={this.onSubmit} noValidate>
            <TextField
              className={formErrors.email.length > 0 ? "error" : null}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus    
              onChange={this.onChange}       
             />
              {formErrors.email.length > 0 && (
                <span className="errorMessage">{formErrors.email}</span>
              )}
            <TextField
              className={formErrors.password.length > 0 ? "error" : null}
              id="outlined-password-input"
              label="Password"
              type="password"
              autoComplete="current-password"
              variant="outlined"
              name="password"
              noValidate
              onChange={this.onChange}
            />
            {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <div className="createAccount">
              <button type="submit">Create Account</button>
              <Grid>
               <Grid item>
                 <Link href="#" variant="body2">
                   {" Forgot password?"}
                 </Link>
               </Grid>
             </Grid>
            <Typography variant="body2" color="textSecondary" align="center">
              {'Copyright © '}
              <Link color="inherit" href="https://material-ui.com/">
                Your Website
              </Link>{' '}
              {new Date().getFullYear()}
              {'.'}
            </Typography>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

export default Login;
