import axios from 'axios';

export const loginadmin = user => {
    return axios
      .post('loginadmin/login', {
        email: user.email,
        password: user.password
      })
      .then(response => {
        localStorage.setItem('usertoken', response.data)
        return response.data
      })
      .catch(err => {
        console.log(err)
      })
  }