import React, { Component } from 'react';
import {Link , Redirect} from 'react-router-dom'
import Navbar from './Navbar';

export default class Admin extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")

        let loggedIn = true
        if(token == null) {
             loggedIn = false
        }

        this.state = {
            loggedIn
        }
    }
  render() {
      if(this.state.loggedIn === false) {
          return <Redirect to ="/api"/>
      }
    return (
      <div>
        <Navbar/>
          <h1> This is an admin page. Only Auth people can see this. </h1>
            <Link to="/api/logout">Logout</Link>
      </div>
    );
  }
}
