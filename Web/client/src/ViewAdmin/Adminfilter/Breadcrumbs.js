import React, { Component } from 'react';
import {BrowserRouter , Link, Switch, Route} from 'react-router-dom';
import Layout from '../Layout';

export default class Breadcrumbs extends Component {
  render() {
    return (
        <BrowserRouter>
        <Layout>
            <Switch>
                <Route exact path='/' render={()=><div>Home wia</div>}/>
                <Route exact path='/Writers' render={()=><div>Writers</div>}/>
            </Switch>
        </Layout>
    </BrowserRouter>
    );
  }
}



