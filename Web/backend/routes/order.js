var express = require('express');
var order = express.Router();
var User = require('../models/order');
// const cors = require('cors')
const jwt = require('jsonwebtoken')
// const bcrypt = require('bcrypt')
/* GET users listing. */
order.get('/', function (req, res, next) {
	User.find({}).then(
		(user) => res.send(user),
		(err) => res.send(err)
	)
});


order.post('/', (req, res) => {
	const newUser = new User(req.body);
	const saveUser = newUser.save().then(
		(user) => res.send(user),
		(err) => res.send(err)
	)
});

order.route('/bill/:id').get(function (req, res) {
    let id = req.params.id;
    User.findById(id, function (err, business){
        res.json(business);
    });
});




module.exports = order;