var express = require('express');
var router = express.Router();
var User = require('../models/product');
// const cors = require('cors')
const jwt = require('jsonwebtoken')
// const bcrypt = require('bcrypt')
/* GET users listing. */
router.get('/', function (req, res, next) {
	User.find({}).then(
		(user) => res.send(user),
		(err) => res.send(err)
	)
});


router.post('/', (req, res) => {
	const newUser = new User(req.body);
	const saveUser = newUser.save().then(
		(user) => res.send(user),
		(err) => res.send(err)
	)
});

router.route('/order/:id').get(function (req, res) {
    let id = req.params.id;
    User.findById(id, function (err, business){
        res.json(business);
    });
});

router.route('/cart/:id').get(function (req, res) {
    User.findByIdAndRemove({_id: req.params.id}, function(err, person){
        if(err) res.json(err);
        else res.json('Successfully removed');
    });
});


module.exports = router;