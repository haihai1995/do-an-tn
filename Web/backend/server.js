var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var app = express()
const mongoose = require('mongoose')
var port = process.env.PORT || 5000

app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

const mongoURI = 'mongodb://localhost:27017/demo'

mongoose
  .connect(
    mongoURI,
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err))

var Users = require('./routes/Users')
var productRouter = require('./routes/products');
var order = require('./routes/order');
// var admins = require('./routes/admins');
var loginadmin = require ('./routes/loginadmin');

app.use('/users', Users)
app.use('/products', productRouter)
app.use('/order',order)
// app.use('/admins',admins)
app.use('/loginadmin',loginadmin)


app.listen(port, function() {
  console.log('Server is running on port: ' + port)
})
