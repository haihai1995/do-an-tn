var mongoose = require('mongoose');
var {Schema, model} = mongoose;

var userSchema = new Schema({
	name: {
		type: String,
	},
	phone: {
		type: Number,
	},
	province: {
		type: String,
	},
	ward: {
		type: String,
	},
	address: {
		type: String,
	},
	date: {
		type: Date,
		default: Date.now
	},
	id: {
        type: String
	},
	price:{
		type:Number,
	}
})

module.exports = order = mongoose.model('order', userSchema)