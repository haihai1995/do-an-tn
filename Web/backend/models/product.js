var mongoose = require('mongoose');
var {Schema, model} = mongoose;

var userSchema = new Schema({
	name: {
		type: String,
	},
	price: {
		type: Number,
	},
	description: {
		type: String,
	},
	imageUrl: {
		type: String,
	},
	currency: {
		type: String,
	},
	id: {
        type: String
	},
	date: {
		type: Date,
		default: Date.now
	},
	amounts:{
		type: Number,
	},
	status: {
		type:Number,
	}
})

module.exports = products = mongoose.model('products', userSchema)